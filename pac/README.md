# Liste des bénéficiaires PAC

## Récupération des donées
Le script python **script.py** permet de lister les montants reversé aux communes dans le cadre de la politique agricole commune.

Le jeu de données nécessaire au bon fonctionnement est téléchargeable directement depuis le site telepac à l'adresse suivante : https://www2.telepac.agriculture.gouv.fr/telepac/tbp/feader/afficher.action

## Dépendences
L'ensemble des librairies python nécessaires au bon fonctionnement du script sont indiquées directement sur le fichier **requirements.txt**.
Pour installer ces dépendences, veuillez taper la commande suivante depuis le dossier pac :

```python
pip install -r requirements.txt
```

## Nettoyage des données
La fonction principale du script est le retraitement des données. Les principales étapes de retraitement sont les suivantes :

### 1. Import des données CSV
Les données des bénéficiaires pac, stockées dans le dossier **pac/data/raw** sous le nom *beneficiaires-pac.csv* sont d'abord lues à l'aide de la librairie python pandas.

### 2. Retraitement
La donnée importée subit ensuite de nombreuses transformations visant à la rendre "propre". Ces transform
sont notamment le reformatage des montants en données numériques et l'identification des valeurs non renseignées (certaines données sont anonymisées).

### 3. Groupement à l'échelle commune
Les données retraitées sont enfin groupées à l'échelle de la commune à l'aide du nom de la commune. Ce groupement permet ensuite de faire la somme des aides totales versées à la communes.