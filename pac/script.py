import pandas as pd

# Chargement du jeu de données
df = pd.read_csv("./data/beneficiaires-pac.csv", sep=";", encoding="latin-1",
                 skipinitialspace=True)
df.drop(columns=["Nom / Raison sociale"], inplace=True)
df.head()

# Conversion des montants en valeur numérique
df["Montant rubrique"] = df["Montant rubrique"].str.replace(",", ".")
df["Montant rubrique"] = df["Montant rubrique"].astype(float)

# Retraitement des codes postaux avec seulement 4 chiffres (car pas de 0 au début)
df["Code postal"] = df["Code postal"].apply(lambda x: "0" + str(x) if len(x) == 4 else str(x))

# Extraction du code département
df["code departement"] = df["Code postal"].str[:2]

# Extraction de la catégorie d'aide
df["catégorie"] = df["Libellé rubrique"].str.split(".").str[0]

# Redéfinition des communes inconnues
df["Commune"] = df["Commune"].replace("XXX", "Inconnu")

# Groupement des données par commune
grouped_df = df[["Commune", "Libellé rubrique", "Montant rubrique"]].groupby(["Commune", "Libellé rubrique"]).sum()

# Export des données retravaillées au format CSV
grouped_df.to_parquet("./data/beneficiaires-pac-communes.gzip")