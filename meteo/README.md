# Données météo par commune sur 10 an

## Dépendences
L'ensemble des librairies python nécessaires au bon fonctionnement du script sont indiquées directement sur le fichier **requirements.txt**.
Pour installer ces dépendences, veuillez taper la commande suivante depuis le dossier "meteo" :

```python
pip install -r requirements.txt
```

## Récupération des données
Le notebook **meteoApiRequest.ipynb** contient l'ensemble des étapes permettant de requêter l'API Open-Meteo (https://open-meteo.com/en/docs/historical-weather-api) et d'obtenir la moyenne des valeurs par mois.

L'API permet d'obtenir les données météorologiques par jour des communes suivantes : 
- **weathercode** : code basé sur les évaluations du National Oceanic and Atmospheric Administration (https://www.nodc.noaa.gov/archive/arc0021/0002199/1.1/data/0-data/HTML/WMO-CODE/WMO4677.HTM) permettant de déterminer la criticité des conditions météorologiques. Plus ce code est élevé et plus la météo peut être considérée comme "critique". 
- **temperature_2m_max** : température maximale observée à deux mètres aux dessus du sol exprimé en °C
- **temperature_2m_min** : température minimale observée à deux mètres aux dessus du sol sur le mois exprimé en °C
- **precipitation_sum** : moyenne des précipitations (pluie et neige) exprimé en mm
- **rain_sum**: moyenne des pluies exprimé en mm
- **snowfall_sum**: moyenne des chutes de neige exprimé en mm
- **precipitation_hours** : nombre d'heure de précipitations  au cours de la journée
- **windspeed_10m_max**: vitesse moyenne du vent, exprimée en km/h
- **windgusts_10m_max**: force moyenne du vent, exprimée en km/h. La force désigne une augmentation soudaine de la vitesse du vent.
- **windirection_10m_dominant**: direction dominante prise par le vent exprimée en °
- **shortwave_radiation_sum**: moyenne des radiation exprimée en Megajoule/m²
- **et0_fao_evapotranspiration**: moyenne de l'évapotranspiration de référence. Cette valeur désigne  la capacité de l'atmosphère à éliminer l'eau de la surface d'un sol avec couvert végétal disposant de l'eau en abondance.

Les données récupérées couvrent les deux dernières années à partir de la d'aujourd'hui. Cette durée peut-être modifiées directement depuis à partir de la ligne `today_minus_10_year = today_date - relativedelta(years=2)` en changeant la valeur du relativedelta par un nombre d'années différent ou en passant sur une durée par mois plutôt que par année (exemple : `relativedelta(month=2)`) 


⚠️ Il est obligatoire d'avoir le fichier csv **communes.csv** enregistrer dans le dossier **meteo/data/** pour que l'API fonctionne correctement. Ce dernier contient les longitudes et latitudes des communes indispensable à la récupération des données.

## Nettoyage des données
Une fois les données de l'API récupérées, les données sont retraitées à plusieurs reprises :

### 1. Extraction du mois et de l'année
A partir de la date, identifiée par la colonne time, une extraction de l'année et du mois est effectuée. 

### 2. Groupement des valeurs
Un groupement des valeurs année, mois, code insee, code postal et nom de commune permet  d'obtenir les valeurs météorologiques moyennes pour chaque commune sur les mois couvrant la période spécifiée.   

### 3. Export des données
Le jeu de donnée obtenu est exporté au format parquet, permettant notamment de créer des fichiers au format zip qui ont l'avantage de prendre moins de place en mémoire qu'un format csv ou xlsx.

## Limites & améliorations
### 1. Requêtage de l'API
Avec plus de 34 955 communes en France, l'étape de requêtage des données météorologiques peut s'avérer laborieuse. En effet, il n'est possible d'effectuer qu'une seule requête à la fois ce qui demande 2 à 3h pour requêter l'ensemble des communes.

De plus, les données ne pouvant être requêtées à la journée, le volume de données à tendance à croître très rapidement. 365 à 366 lignes sont récupérées par commune par an, ce qui nous amène à un total de 12.6 Millions de valeurs par année environ. Il est donc indispensable d'avoir suffisament de RAM et d'espace disque avant de lancer le script, sinon il y a un risque que le notebook ne puisse finaliser son exécution correctement.

### 2. Pertinence des données
La récupération des données peut être améliorées avec la création de nouvelles données à partir de celles obtenues via l'API Open-Meteo :
- **le nombre de jours de pluies par mois** : compter le nombre de jours ou la precipitation_sum est égale à 0 dans le mois
- **le dernier jour de gel avant l'été** : identifier le dernier jour où la température passe sous les 0°C au cours de l'année
- **le nombre de jour où le vent souffle dans une direction chaque mois** : identifier la direction du vent à partir de la métrique windirection_10m_dominant et attribuer une répartition par orientation Nord-Sud-Est-Ouest