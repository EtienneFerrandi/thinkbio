import pandas as pd
import requests as requests

pd.options.display.max_columns = 50

# Récupération de la liste des communes
communes_df = pd.read_csv("./data/communes.csv",
                          usecols=["code_commune_INSEE", "nom_commune_postal", "code_postal", "longitude", "latitude"])


# Requêtage de l'API Open-Meteo : https://open-meteo.com/
def get_city_weather_data(row, start_date, end_date):
    URL = "https://archive-api.open-meteo.com/v1/archive"
    payload = {
        "longitude": row["longitude"],
        "latitude": row["latitude"],
        "start_date": start_date,
        "end_date": end_date,
        "daily": "weathercode,temperature_2m_mean,precipitation_sum,rain_sum,snowfall_sum,precipitation_hours",
        "timezone": "Europe/Berlin"
    }
    res = requests.get(URL, params=payload)

    try:
        data = res.json()["daily"]
        data["commune"] = row["nom_commune_postal"]
        data["code_insee"] = row["code_commune_INSEE"]
        data["code_postal"] = row["code_postal"]
        data["longitude"] = row["longitude"]
        data["latitude"] = row["latitude"]
        print(f"Code postal:", row["code_postal"])
        return data
    except:
        print(f"Pas de données disponible pour la commune {row['nom_commune_postal']}")


# Création du tableau de données
data_list = []

for index, row in communes_df[:2].iterrows():
    data = get_city_weather_data(row, start_date="2021-01-01", end_date="2022-12-31")
    data_list.append(data)

# Création du dataframe
df = pd.DataFrame(data_list)
exploded_df = df.explode(["weathercode", "time", "temperature_2m_mean", "precipitation_sum", "rain_sum", "snowfall_sum", "precipitation_hours"])

# Export des données au format csv
exploded_df.to_csv("./data/donnees-meteo.csv", index=False)

