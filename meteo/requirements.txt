pandas==2.0.1
python_dateutil==2.8.2
Requests==2.31.0
pyarrow==12.0.0
jupyter==1.0.0
