# Challenge GD4H - ThinkBio

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Elle organise un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées.

Liens : 
<a href="https://challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Site</a> / 
<a href="https://forum.challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Forum</a>
a

## ThinkBio

De nombreuses données environnementales existent dans des mailles temporelles et spatiales spécifiques. Ce défi vise à systématiser l’appariement entre bases de données à un niveau cohérent, de manière documentée et en facilitant l’expansion de ce travail d’appariement vers de nouvelles sources de données.

La finalité du projet est de permettre des analyses d’impact de l’utilisation de produits phytosanitaires sur des variables d’éducation et de santé.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=45" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

>TODO / **Description Solution**

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

>TODO / **documentation d'utilisation de la solution**

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).